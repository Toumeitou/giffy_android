package com.fullcontact.giffy;


import android.content.Context;
import android.content.SharedPreferences;

import com.fullcontact.giffy.GiffyService.AccessToken;


public class UserCache {
    public static final String PREFERENCES_NAME = "userInfo";
    public static final String KEY_ACCESS_TOKEN = "accessToken";
    public static final String KEY_ACCOUNT_ID = "accountId";
    private SharedPreferences preferences;

    public UserCache(Context context) {
        preferences = context.getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE);
    }

    private SharedPreferences getPreferences() {
        return preferences;
    }

    public void cacheAccessToken(AccessToken accessToken) {
        preferences.edit()
            .putString(KEY_ACCESS_TOKEN, accessToken.accessToken)
            .putString(KEY_ACCOUNT_ID, accessToken.accountId)
            .apply();
    }

    public AccessToken getAccessToken() {
        if (!preferences.contains(KEY_ACCESS_TOKEN) || !preferences.contains(KEY_ACCOUNT_ID)) {
            return null;
        }
        return AccessToken.builder()
            .accessToken(preferences.getString(KEY_ACCESS_TOKEN, null))
            .accountId(preferences.getString(KEY_ACCOUNT_ID, null))
            .build();
    }
}
