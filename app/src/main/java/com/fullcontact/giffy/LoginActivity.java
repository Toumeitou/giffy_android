package com.fullcontact.giffy;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit.RetrofitError;
import rx.android.schedulers.AndroidSchedulers;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;


public class LoginActivity extends AppCompatActivity {
    private static final String TAG = LoginActivity.class.getSimpleName();

    @Bind(R.id.email) EditText email;
    @Bind(R.id.username) EditText username;
    @Bind(R.id.password) EditText password;

    @Bind(R.id.signUp) Button signUp;
    @Bind(R.id.signIn) Button signIn;

    @Bind(R.id.switchMode) TextView switchMode;
    private GiffyClient giffyClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);
        ButterKnife.bind(this);

        signUp.setOnClickListener(setToSignUpMode());
        signIn.setOnClickListener(setToSignInMode());

        giffyClient = new GiffyClient(new UserCache(this));
        if (giffyClient.isLoggedIn()) {
            startMainActivity();
        }
    }

    private void startMainActivity() {
        startActivity(new Intent(this, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
    }

    private View.OnClickListener setToSignUpMode() {
        return __ -> {
            if (!isEmail(email.getText())) {
                username.setText(email.getText());
                email.setText("");
            }
            setVisibilities(true);
            switchMode.setOnClickListener(setToSignInMode());
            signUp.setOnClickListener(trySignUp());
        };
    }

    private View.OnClickListener setToSignInMode() {
        return __ -> {
            setVisibilities(false);
            switchMode.setOnClickListener(setToSignUpMode());
            signIn.setOnClickListener(trySignIn());
        };
    }

    private void setVisibilities(boolean signUpMode) {
        email.setVisibility(VISIBLE);
        email.setHint(signUpMode ? R.string.email : R.string.emailOrUsername);
        username.setVisibility(signUpMode ? VISIBLE : GONE);
        password.setVisibility(VISIBLE);
        signUp.setVisibility(signUpMode ? VISIBLE : GONE);
        signUp.setText(R.string.proceed);
        signIn.setVisibility(!signUpMode ? VISIBLE : GONE);
        signIn.setText(R.string.proceed);
        switchMode.setVisibility(VISIBLE);
        switchMode.setText(signUpMode ? R.string.haveAccount : R.string.needAccount);
    }

    private boolean isEmail(CharSequence text) {
        return Patterns.EMAIL_ADDRESS.matcher(text).matches();
    }

    public static class ErrorResponse {
        public String status_code;
        public String message;
        public String error;
    }

    private void notifyError(Throwable throwable) {
        Log.e(TAG, "error", throwable);
        if (throwable instanceof RetrofitError) {
            ErrorResponse errorResponse = (ErrorResponse) ((RetrofitError) throwable).getBodyAs(ErrorResponse.class);
            Toast.makeText(this, errorResponse.message, Toast.LENGTH_SHORT).show();
        }
        else {
            Toast.makeText(this, throwable.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private View.OnClickListener trySignUp() {
        return __ -> {
            signUp.setEnabled(false);
            switchMode.setEnabled(false);
            giffyClient.register(email.getText().toString(), username.getText().toString(), password.getText().toString())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnTerminate(() -> {
                    signUp.setEnabled(true);
                    switchMode.setEnabled(true);
                })
                .subscribe(
                    userInfo -> {
                        Toast.makeText(LoginActivity.this, userInfo.username + " registered", Toast.LENGTH_SHORT).show();
                        startMainActivity();
                    },
                    this::notifyError);
        };
    }

    private View.OnClickListener trySignIn() {
        return __ -> {
            signIn.setEnabled(false);
            switchMode.setEnabled(false);
            giffyClient.logIn(email.getText().toString(), password.getText().toString())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnTerminate(() -> {
                    signIn.setEnabled(true);
                    switchMode.setEnabled(true);
                })
                .subscribe(
                    userInfo -> {
                        Toast.makeText(LoginActivity.this, userInfo.username + " logged in", Toast.LENGTH_SHORT).show();
                        startMainActivity();
                    },
                    this::notifyError);
        };
    }
}
