package com.fullcontact.giffy;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.fullcontact.giffy.FeedFragment.FeedFragmentListener;
import com.fullcontact.giffy.FeedFragment.FeedType;
import com.fullcontact.giffy.GiffyService.GifInfo;
import com.fullcontact.giffy.GiffyService.UserInfo;
import com.fullcontact.giffy.UsersFragment.UserFragmentListener;

import java.io.File;
import java.util.List;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;

import static com.fullcontact.giffy.FeedFragment.FeedType.All;
import static com.fullcontact.giffy.FeedFragment.FeedType.User;


public class MainActivity extends AppCompatActivity implements
    UserFragmentListener,
    FeedFragmentListener {

    public static final String TAG = MainActivity.class.getSimpleName();
    GiffyClient giffyClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        replaceFragment(FeedFragment.newInstance(All));
        giffyClient = new GiffyClient(new UserCache(this));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                return true;
            case R.id.action_signout:
                giffyClient.logout();
                startActivity(new Intent(this, LoginActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                return true;
            case R.id.action_gifs:
                replaceFragment(FeedFragment.newInstance(All));
                return true;
            case R.id.action_tags:
                replaceFragment(TagsFragment.newInstance());
                return true;
            case R.id.action_users:
                replaceFragment(UsersFragment.newInstance());
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void replaceFragment(Fragment fragment) {
        getFragmentManager().beginTransaction().replace(R.id.contents, fragment).commit();
    }

    @Override
    public Observable<List<UserInfo>> getUserList() {
        return giffyClient.getUsers();
    }

    @Override
    public Observable<List<UserInfo>> getMyFollowingList() {
        return giffyClient.giffyService.following(giffyClient.accessToken.accountId);
    }

    @Override
    public void userClicked(UserInfo userInfo) {
        replaceFragment(FeedFragment.newInstance(User, userInfo.id.toString()));
    }

    @Override
    public Observable<String> changeFollowing(UserInfo userInfo) {
        return userInfo.following
            ? giffyClient.giffyService.follow(GiffyService.UserToFollow.builder().user_id(userInfo.id).build())
            : giffyClient.giffyService.unfollow(userInfo.id.toString());
    }

    @Override
    public Observable<List<GifInfo>> getFeed(FeedType type, String extra) {
        switch (type) {
            case Following: return giffyClient.giffyService.getFeedFollowing();
            case Tag: return giffyClient.giffyService.getFeedForTag(extra);
            case User: return giffyClient.giffyService.getFeedForUser(extra);
            default: return giffyClient.giffyService.getFeedAll();
        }
    }

    @Override
    public void uploadGif(File file, String description) {
        giffyClient.uploadGif(file, description)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                gifInfo -> Toast.makeText(this, gifInfo.filename, Toast.LENGTH_SHORT).show(),
                throwable -> Log.e(TAG, "gif upload failed", throwable));
    }

    @Override
    public Observable<File> downloadGif(String filename) {
        return giffyClient.getGif(filename, getExternalFilesDir("images").getAbsolutePath());
    }
}
