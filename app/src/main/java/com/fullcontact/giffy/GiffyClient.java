package com.fullcontact.giffy;


import android.util.Log;
import android.util.Patterns;

import com.fullcontact.giffy.GiffyService.AccessToken;
import com.fullcontact.giffy.GiffyService.UploadInfo;
import com.fullcontact.giffy.GiffyService.UserCredentials;
import com.fullcontact.giffy.GiffyService.UserCredentials.UserCredentialsBuilder;
import com.fullcontact.giffy.GiffyService.UserInfo;

import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import lombok.AccessLevel;
import lombok.Getter;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.mime.TypedFile;
import rx.Observable;


public class GiffyClient {

    private static final String TAG = GiffyClient.class.getSimpleName();
    private static String ENDPOINT = "http://192.168.1.114:8080";
    private final UserCache userCache;
    private RestAdapter restAdapter;
    public GiffyService giffyService;
    @Getter(AccessLevel.PRIVATE)
    AccessToken accessToken = null;

    public GiffyClient(UserCache userCache) {
        this.userCache = userCache;
        refreshToken();
        restAdapter = new RestAdapter.Builder()
            .setEndpoint(ENDPOINT)
            .setLogLevel(RestAdapter.LogLevel.HEADERS_AND_ARGS)
            .setRequestInterceptor(requestInterceptor)
            .build();
        giffyService = restAdapter.create(GiffyService.class);
    }

    public void refreshToken() { // TODO: refresh if necessary
        AccessToken cachedToken = userCache.getAccessToken();
        if (cachedToken != null) {
            setAccessToken(cachedToken);
        }
    }

    public void logout() {
        setAccessToken(new AccessToken(null, null));
    }

    private void setAccessToken(AccessToken accessToken) {
        this.accessToken = accessToken;
        userCache.cacheAccessToken(accessToken);
    }

    private RequestInterceptor requestInterceptor = request -> {
        if (accessToken == null) return;
        request.addHeader("X-Giffy-AccessToken", accessToken.accessToken);
        request.addHeader("X-Giffy-AccountId", accessToken.accountId);
    };

    public Observable<UserInfo> logIn(String usernameOrEmail, String password) {
        UserCredentialsBuilder builder = UserCredentials.builder();
        if (isEmail(usernameOrEmail)) {
            builder.email(usernameOrEmail);
        }
        else {
            if (usernameOrEmail.length() < 3) {
                return error("Username must be at least 3 symbols long");
            }
            builder.username(usernameOrEmail);
        }
        if (password.length() < 8) {
            return error("Password must be 8 symbols long!");
        }
        builder.password(password);

        return giffyService.login(builder.build())
            .concatMap(accessToken -> {
                setAccessToken(accessToken);
                return giffyService.getUser(accessToken.accountId);
            });
    }

    private boolean isEmail(String usernameOrEmail) {
        return Patterns.EMAIL_ADDRESS.matcher(usernameOrEmail).matches();
    }

    <T> Observable<T> error(String errorMessage) {
        return Observable.error(new Throwable(errorMessage));
    }

    public Observable<UserInfo> register(String email, String username, String password) {
        if (!isEmail(email)) {
            return error("Invalid email format!");
        }
        if (username.length() < 3) {
            return error("Username must be at least 3 symbols long");
        }
        if (password.length() < 8) {
            return error("Password must be 8 symbols long!");
        }

        UserCredentials userCredentials = UserCredentials.builder().email(email).username(username).password(password).build();
        return giffyService.register(userCredentials)
            .concatMap(accessToken -> {
                setAccessToken(accessToken);
                return giffyService.getUser(accessToken.accountId);
            });
    }

    public boolean isLoggedIn() {
        return userCache.getAccessToken() != null;
    }

    public Observable<List<UserInfo>> getUsers() {
        return giffyService.getUsers();
    }

    public Observable<UploadInfo> uploadGif(File file, String description) {
        return giffyService.uploadGif(new TypedFile("image/gif", file), description);
    }

    // TODO: get by id
    public Observable<File> getGif(String remoteFilename, String localFilesDir) {
        return giffyService.getGif(remoteFilename)
            .map(response -> {
                try {
                    InputStream inputStream = response.getBody().in();
                    File file = new File(localFilesDir, remoteFilename);
                    FileOutputStream output = new FileOutputStream(file, false);
                    IOUtils.copy(inputStream, output);
                    output.close();
                    return file;
                } catch (IOException e) {
                    Log.e(TAG, "failed to download gif", e);
                    return null; // TODO: handle
                }
            });
    }
}
