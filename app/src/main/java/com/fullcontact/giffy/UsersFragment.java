package com.fullcontact.giffy;


import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.fullcontact.giffy.GiffyService.UserInfo;
import com.google.common.collect.FluentIterable;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;

import static android.support.v7.widget.LinearLayoutManager.VERTICAL;


public class UsersFragment extends Fragment {
    @Bind(R.id.users) RecyclerView userList;
    UserFragmentListener listener;
    private static final String TAG = UsersFragment.class.getSimpleName();

    public static UsersFragment newInstance() {
        return new UsersFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.users_fragment, container, false);
        ButterKnife.bind(this, root);

        userList.setLayoutManager(new LinearLayoutManager(getActivity(), VERTICAL, false));

        listener.getMyFollowingList().flatMap(
            followedUsers -> listener.getUserList()
                .map(allUsers -> (List<UserInfo>) FluentIterable.from(allUsers)
                    .transform(user -> user.withFollowing(followedUsers.contains(user))).toList()))
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                users -> userList.setAdapter(new UserInfoAdapter(users)),
                throwable -> Log.e(TAG, "failed to get users", throwable));
        return root;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        listener = (UserFragmentListener) activity;
    }

    private class UserInfoHolder extends RecyclerView.ViewHolder {
        TextView username;
        TextView email;
        ImageView following;
        UserInfo cachedUser;

        public UserInfoHolder(View itemView) {
            super(itemView);
            username = ButterKnife.findById(itemView, R.id.username);
            email = ButterKnife.findById(itemView, R.id.email);
            following = ButterKnife.findById(itemView, R.id.following);
        }

        public void setUser(UserInfo user) {
            cachedUser = user;
            username.setText(user.username);
            email.setText(user.email);
            itemView.setOnClickListener(__ -> listener.userClicked(cachedUser));
            updateFollowing();
            following.setOnClickListener(__ -> {
                cachedUser = cachedUser.withFollowing(!cachedUser.following);
                listener.changeFollowing(cachedUser)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                        dummy -> updateFollowing(),
                        throwable -> Log.e(TAG, "failed to set following", throwable));
            });
        }

        private void updateFollowing() {
            following.setImageResource(cachedUser.following ? R.drawable.ic_favorite_black_24dp : R.drawable.ic_favorite_border_black_24dp);
        }
    }

    private class UserInfoAdapter extends RecyclerView.Adapter<UserInfoHolder> {

        private final List<UserInfo> users;

        public UserInfoAdapter(List<UserInfo> users) {
            this.users = users;
        }

        @Override
        public UserInfoHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new UserInfoHolder(View.inflate(getActivity(), R.layout.user_entry, null));
        }

        @Override
        public void onBindViewHolder(UserInfoHolder holder, int position) {
            holder.setUser(users.get(position));
        }

        @Override
        public int getItemCount() {
            return users.size();
        }
    }

    public interface UserFragmentListener {
        Observable<List<UserInfo>> getUserList();
        Observable<List<UserInfo>> getMyFollowingList();
        void userClicked(UserInfo userInfo);
        Observable<String> changeFollowing(UserInfo userInfo);
    }
}
