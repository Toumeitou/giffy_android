package com.fullcontact.giffy;


import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;


public class TagsFragment extends Fragment {
    public static TagsFragment newInstance() {
        return new TagsFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.tags_fragment, container, false);
        ButterKnife.bind(this, root);

        return root;
    }
}
