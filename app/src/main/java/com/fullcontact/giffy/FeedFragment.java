package com.fullcontact.giffy;


import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fullcontact.giffy.GiffyService.GifInfo;
import com.nononsenseapps.filepicker.FilePickerActivity;

import java.io.File;
import java.io.IOException;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import pl.droidsonroids.gif.GifDrawable;
import pl.droidsonroids.gif.GifImageView;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;


public class FeedFragment extends Fragment {

    private static final String TAG = FeedFragment.class.getSimpleName();
    public static final String ARG_PATH = "path";
    public static final int REQ_PICK_GIF = 13;
    public static final String ARG_EXTRA_PARAM = "extraParam";
    FeedFragmentListener listener;
    @Bind(R.id.gifs) RecyclerView gifsList;
    @Bind(R.id.addGif) FloatingActionButton addGif;

    public enum FeedType {
        All(""),
        Following("/following"),
        Tag("/tag/"),
        User("/user/");

        public final String path;

        FeedType(String path) {
            this.path = path;
        }
    }

    public static FeedFragment newInstance(FeedType type) {
        return newInstance(type, "");
    }

    public static FeedFragment newInstance(FeedType type, String extraParam) {
        FeedFragment fragment = new FeedFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PATH, type.name());
        args.putString(ARG_EXTRA_PARAM, extraParam);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.feed_fragment, container, false);
        ButterKnife.bind(this, root);

        gifsList.setLayoutManager(new LinearLayoutManager(getActivity()));

        Bundle args = getArguments();
        if (args == null) return root;

        String path = args.getString(ARG_PATH);
        String extraParam = args.getString(ARG_EXTRA_PARAM);
        listener.getFeed(FeedType.valueOf(path), extraParam)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                gifs -> gifsList.setAdapter(new GifAdapter(gifs)),
                throwable -> Log.e(TAG, "Failed to load feed at " + path + extraParam, throwable));

        addGif.setOnClickListener(__ -> showImagePicker());

        return root;
    }

    private void showImagePicker() {
        startActivityForResult(new Intent(getActivity(), FilePickerActivity.class), REQ_PICK_GIF);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if (requestCode != REQ_PICK_GIF) return;
        Uri uri = intent.getData();
        listener.uploadGif(new File(uri.getPath()), "This shall be a description");
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        listener = (FeedFragmentListener) activity;
    }

    private class GifHolder extends RecyclerView.ViewHolder {
        GifImageView gif;
        public GifHolder(View itemView) {
            super(itemView);
            gif = ButterKnife.findById(itemView, R.id.gif);
        }

        private void setImageView(File data) {
            try {
                gif.setImageDrawable(new GifDrawable(data));
            } catch (IOException e) {
                Log.e(TAG, "failed to set image", e);
            }
        }

        public void setGif(String filename) {
            listener.downloadGif(filename)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    this::setImageView,
                    throwable -> Log.e(TAG, "failed to download gif", throwable));
        }
    }

    private class GifAdapter extends RecyclerView.Adapter<GifHolder> {
        private final List<GifInfo> gifs;

        public GifAdapter(List<GifInfo> gifs) {
            this.gifs = gifs;
        }
        @Override
        public GifHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new GifHolder(View.inflate(getActivity(), R.layout.feed_entry, null));
        }

        @Override
        public void onBindViewHolder(GifHolder holder, int position) {
            holder.setGif(gifs.get(position).filename);
        }

        @Override
        public int getItemCount() {
            return gifs.size();
        }
    }

    public interface FeedFragmentListener {
        Observable<List<GifInfo>> getFeed(FeedType type, String extra);
        void uploadGif(File file, String description);
        Observable<File> downloadGif(String filename);
    }
}
