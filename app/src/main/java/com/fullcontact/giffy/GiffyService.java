package com.fullcontact.giffy;


import java.util.List;

import lombok.Builder;
import retrofit.client.Response;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;
import retrofit.http.Path;
import retrofit.http.Query;
import retrofit.http.Streaming;
import retrofit.mime.TypedFile;
import rx.Observable;


public interface GiffyService {
    //================================================================================
    // Users
    //================================================================================

    @POST("/users/login")
    Observable<AccessToken> login(@Body UserCredentials userCredentials);

    @POST("/users")
    Observable<AccessToken> register(@Body UserCredentials userCredentials);

    @GET("/users/{id}")
    Observable<UserInfo> getUser(@Path("id") String id);

    @GET("/users")
    Observable<List<UserInfo>> getUsers();

    @Builder
    class AccessToken {
        public String accessToken;
        public String accountId;
    }

    @Builder
    class UserCredentials {
        public final String email;
        public final String username;
        public final String password;
    }

    @Builder
    class UserInfo {
        public final Integer id;
        public final String username;
        public final String email;
        public final boolean following;
        public UserInfo withFollowing(boolean newValue) {
            return UserInfo.builder().id(id).username(username).email(email).following(newValue).build();
        }

        @Override
        public boolean equals(Object o) {
            if (!(o instanceof UserInfo)) return false;
            return ((UserInfo) o).id.equals(id);
        }
    }

    //================================================================================
    // Images
    //================================================================================

    @Multipart
    @POST("/gifs")
    Observable<UploadInfo> uploadGif(@Part("image") TypedFile file, @Query("description") String description);

    @GET("/feed")
    Observable<List<GifInfo>> getFeedAll();

    @GET("/feed/following")
    Observable<List<GifInfo>> getFeedFollowing();

    @GET("/feed/user/{id}")
    Observable<List<GifInfo>> getFeedForUser(@Path("id") String userId);

    @GET("/feed/tag/{id}")
    Observable<List<GifInfo>> getFeedForTag(@Path("id") String tagId);

    @GET("/static/uploads/{filename}")
    @Streaming
    Observable<Response> getGif(@Path("filename") String filename);

    @Builder
    class UploadInfo {
        public final String imageId;
        public final String filename;
    }

    @Builder
    class GifInfo {
        public final String description;
        public final String user_id;
        public final String id;
        public final String filename;
    }

    //================================================================================
    // Following
    //================================================================================

    @POST("/follow")
    Observable<String> follow(@Body UserToFollow userToFollow);

    @DELETE("/follow/{id}")
    Observable<String> unfollow(@Path("id") String userId);

    @Builder
    class UserToFollow {
        public final Integer user_id;
    }

    @GET("/following/{id}")
    Observable<List<UserInfo>> following(@Path("id") String userId);

    @GET("/followers/{id}")
    Observable<List<UserInfo>> followers(@Path("id") String userId);
}
